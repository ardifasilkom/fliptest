<?php

require_once('./database/config.php');
require_once('./database/user_table.php');
require_once('./database/disbursement_table.php');
require_once('./model/User.php');

class Migration
{
	public function migrate()
	{
		try {

			$config = new Config();
		    $conn = new PDO("mysql:host=$config->servername;", $config->username, $config->password);
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		    $database = "CREATE DATABASE  `".$config->dbname."` ";
		    $conn->exec($database);
			echo "Database created successfully \n";
			$conn = null;

			$user_table = new UserTable();
			$disbursement_table = new DisbursementTable();
			$user_table->create();
			$disbursement_table->create();

			$user = new User();
			$user->name = "Rachmat Ardiansyah";
			$user->email = "ardifasilkom@gmail.com";
			$user->save();

	    } catch(PDOException $e)
	    {
			echo "Migration Failed";
	    }
	}
}

$migration = new Migration();
$migration->migrate();
?>