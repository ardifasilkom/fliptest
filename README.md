# FlipTest

API to call SlightlyBig-Flip API.
By Default, this API using PHP 7 and MySQL Database.
For database config can be change at database/config.php
This connection using PDO and it can be change on any SQL DB.

Since https://nextar.flip.id/ is down, developer use they localhost to build this API. Don't hesitate to contact them.

First Thing First !!
- Please Migrate and Seed the database fisrt using command php migration.php
- Run php createRequest.php to create disbursement request to Slighly big flip API.
- Run php getStatus.php to create disbursement request to Slighly big flip API.