<?php 
require_once('./database/config.php');
require_once('./database/user_table.php');
require_once('./database/disbursement_table.php');
require_once('./model/User.php');
require_once('./model/Transaction.php');

class Disbursement
{
	public function requestDisbursement($bank_code, $account_number, $amount, $remark)
	{
		try {
			$host = "https://nextar.flip.id/disburse/5535152564";
			$username = "HyzioY7LP6ZoO7nTYKbG8O4ISkyWnX1JvAEVAhtWKZumooCzqp41";
			$password = null;
			$data = [
				'bank_code' => $bank_code,
				'account_number' => $account_number,
				'amount' => $amount,
				'remark' => $remark,
			];
			$data_string = json_encode($data); 
			$ch = curl_init($host);
			curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
			curl_setopt($ch, CURLOPT_HEADER, 1);
			curl_setopt($ch, CURLOPT_USERPWD, $username . ":" . $password);
			curl_setopt($ch, CURLOPT_TIMEOUT, 30);
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			$return = curl_exec($ch);
			curl_close($ch);
			echo "Success to make Disbursement Request\n";
			echo $return;
		} catch (\Exception $e) {
			echo "Error create request";
		}
	}

	public function getDisbursementData()
	{
		try {
			$username = 'HyzioY7LP6ZoO7nTYKbG8O4ISkyWnX1JvAEVAhtWKZumooCzqp41';
			$password = null;
			$url = 'https://nextar.flip.id/disburse/5535152564';
			$context = stream_context_create(array(
			    'http' => array(
			        'header'  => array(
			        	"Authorization: Basic " . base64_encode("$username:$password"),
			        	'Content-Type: application/x-www-form-urlencoded',
			        )
			    )
			));
			$data = file_get_contents($url, false, $context);
			$decoded_data = json_decode($data, true);
			if($decoded_data)
			{
				$transaction = new Transaction();
				$transaction->user_id = 1;
				$transaction->transaction_id = $decoded_data['id'];
				$transaction->amount = $decoded_data['amount'];
				$transaction->status = $decoded_data['status'];
				$transaction->time_stamp = $decoded_data['timestamp'];
				$transaction->bank_code = $decoded_data['bank_code'];
				$transaction->account_number = $decoded_data['account_number'];
				$transaction->beneficiary_name = $decoded_data['beneficiary_name'];
				$transaction->remark = $decoded_data['remark'];
				$transaction->receipt = $decoded_data['receipt'];
				$transaction->time_served = $decoded_data['time_served'];
				$transaction->fee = $decoded_data['fee'];
				$transaction->save();
				
			} else{echo "Failed to get data";}
		} catch (\Exception $e) {
			echo "Error get data";
		}
	}
}

?>
