<?php 
class UserTable
{
	public function create()
	{
		require_once('config.php');
		$config = new Config();

		try {

			$conn = new PDO("mysql:host=$config->servername;dbname=$config->dbname", $config->username, $config->password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$user_table = "CREATE TABLE IF NOT EXISTS user (
			    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			    name VARCHAR(30),
			    email VARCHAR(50)
		    )";
		    $conn->exec($user_table);
			echo "Success create User table \n";
			$conn = null;

		} catch (\Exception $e) {
			echo "Create table failed";
		}
	}
}
?>