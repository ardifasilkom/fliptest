<?php 
class DisbursementTable
{
	public function create()
	{
		require_once('config.php');
		$config = new Config();

		try {

			$conn = new PDO("mysql:host=$config->servername;dbname=$config->dbname", $config->username, $config->password);
			$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		   	$disbursement_table = "CREATE TABLE IF NOT EXISTS disbursement (
				id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
			    user_id INT(6),
			    transaction_id VARCHAR(12),
			    amount INT(12),
			    status VARCHAR(50),
			    time_stamp TIMESTAMP,
			    bank_code VARCHAR(30),
			    account_number VARCHAR(12),
			    beneficiary_name VARCHAR(30),
			    remark VARCHAR(30),
			    receipt TEXT(255),
			    time_served TIMESTAMP,
			    fee INT(12),
			    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
		    )";
		    $conn->exec($disbursement_table);
			echo "Success create Disbursement table \n";
			$conn = null;

		} catch (\Exception $e) {
			echo "Create table failed";
		}
	}
}
?>